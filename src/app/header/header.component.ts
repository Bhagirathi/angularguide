import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth/auth.service';

import { DataStorageService } from '../shared/data-storage.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit ,OnDestroy {
  constructor(private dataStorageService: DataStorageService,private auth:AuthService) {}
  userSub : Subscription;
  isAuthenticated = false;
  ngOnInit(){
    this.userSub =  this.auth.user.subscribe(user=>{
      this.isAuthenticated = !!user //or !user ? false : true
      
    });
  }
  onSaveData() {
    this.dataStorageService.storeRecipes();
  }

  onFetchData() {
    this.dataStorageService.fetchRecipes().subscribe();
  }
  ngOnDestroy(){
    this.userSub.unsubscribe();
  }
  Logout(){
    this.auth.logOut();
  }
}
