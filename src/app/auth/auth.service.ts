import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { BehaviorSubject, Subject, throwError } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { User } from "./user.model";

export interface AuthResponseData {
  idToken: string;
  email: string;
  refreshToken: string;
  expiresIn: string;
  localId: string;
  registered?: boolean;
}
@Injectable({ providedIn: "root" })
export class AuthService {
  constructor(private http: HttpClient, private router: Router) {}

  user = new BehaviorSubject<User>(null);
  tokenExpTimer: any;
  signUp(email: string, password: string) {
    return this.http
      .post<AuthResponseData>(
        "https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyAEsjGt4loxZ3xTPLCP0yRIFVVIOtaGWfY",
        { email: email, password: password, returnSecureToken: true }
      )
      .pipe(
        catchError(this.handleError),
        tap((resData) => {
          this.handleAuth(
            resData.email,
            resData.localId,
            resData.idToken,
            +resData.expiresIn
          );
        })
      );
  }
  logIn(email: string, password: string) {
    return this.http
      .post<AuthResponseData>(
        "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyAEsjGt4loxZ3xTPLCP0yRIFVVIOtaGWfY",
        { email: email, password: password, returnSecureToken: true }
      )
      .pipe(
        catchError(this.handleError),
        tap((resData) => {
          this.handleAuth(
            resData.email,
            resData.localId,
            resData.idToken,
            +resData.expiresIn
          );
        })
      );
  }
  autoLogin() {
    const userData: {
      email: string;
      id: string;
      _token: string;
      _tokenExpirationDate: string;
    } = JSON.parse(localStorage.getItem("userData"));
    if (!userData) {
      return;
    }
    const loginUser = new User(
      userData.email,
      userData.id,
      userData._token,
      new Date(userData._tokenExpirationDate)
    );
    if (loginUser.token) {
      this.user.next(loginUser);
      const expDuration =
        new Date(userData._tokenExpirationDate).getTime() -
        new Date().getTime();
      this.autoLogOut(expDuration);
    }
  }
  private handleError(errorRes: HttpErrorResponse) {
    console.log(errorRes);

    let errorMsg = "An Unknown Error Occured";
    if (!errorRes.error || !errorRes.error.error) {
      return throwError(errorMsg);
    }
    switch (errorRes.error.error.message) {
      case "EMAIL_EXISTS":
        errorMsg = "This Email Exist Already";
        break;
      case "EMAIL_NOT_FOUND":
        errorMsg = "Please Enter a valid mail";
        break;
      case "INVALID_PASSWORD":
        errorMsg = "Please Enter Correct Password";
        break;
      default:
        errorMsg = " An Error Occured";
    }
    return throwError(errorMsg);
  }

  public handleAuth(
    email: string,
    userId: string,
    token: string,
    expIn: number
  ) {
    const expDate = new Date(new Date().getTime() + +expIn * 1000);
    const user = new User(email, userId, token, expDate);
    this.user.next(user);
    localStorage.setItem("userData", JSON.stringify(user));
    this.autoLogOut(expIn * 1000);
  }
  logOut() {
    this.user.next(null);
    this.router.navigate(["/auth"]);
    localStorage.removeItem("userData");
    if (this.tokenExpTimer) {
      clearTimeout(this.tokenExpTimer);
    }
    this.tokenExpTimer = null;
  }
  autoLogOut(expTime: number) {
      console.log("expTime--->",expTime);
      
    this.tokenExpTimer = setTimeout(() => {
      this.logOut();
    }, expTime);
  }
}
