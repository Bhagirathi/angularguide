import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthResponseData, AuthService } from './auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  isLoginMode = true;
  isLoading = false;
  error = null;
  constructor(private authService: AuthService, private route:Router) { }

  ngOnInit(): void {
  }
onSwitchMode (){
  this.isLoginMode = !this.isLoginMode
} ;
onSubmit(form:NgForm){
console.log('FormData---->',form.value);
if(!form.valid)return;

const email = form.value.email;
const password = form.value.password;
this.isLoading = true;
let authObs : Observable<AuthResponseData>;
if(this.isLoginMode){
  authObs =this.authService.logIn(email,password);
}else{
  authObs =this.authService.signUp(email,password);
}
authObs.subscribe(resData=>{
  console.log('Res Data--->',resData);  
  this.isLoading = false;
  this.route.navigate(['/recipes']);
},errorMsg =>{
  console.log('errorMsg--->',errorMsg);
  this.error = errorMsg;
  this.isLoading = false;
})
// if(this.isLoginMode){
// this.authService.logIn(email,password).subscribe( responseData =>{
//   console.log('responseData-->',responseData);
//   this.isLoading = false;
// },error=>{
//   this.error = error;
//   this.isLoading = false;
//   console.log('error==>',error);
  
// })
// }else{
//   this.authService.signUp(email,password).subscribe( responseData =>{
//     console.log('responseData-->',responseData);
//     this.isLoading = false;
//   },error=>{
//     this.error = error;
//     this.isLoading = false;
//     console.log('error==>',error);
    
//   })
// }

form.reset();
}
}
